import { Component, OnInit } from '@angular/core';
import { EscuelasService } from '../../services/escuelas.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
declare var swal: any;
@Component({
  selector: 'app-bienvenidos',
  templateUrl: './bienvenidos.component.html',
  styleUrls: ['./bienvenidos.component.scss']
})
export class BienvenidosComponent implements OnInit {
  validatingForm: FormGroup;

  constructor(
    private _router: Router
  ) {
    
  }

  ngOnInit() {
    this.validatingForm = new FormGroup({
      modalFormLoginEmail: new FormControl('', [Validators.required,]),
      modalFormLoginPassword: new FormControl('', Validators.required)
    });
  }
  onSubmit(){
    if (this.validatingForm.valid) {
      
      
    }
  }
  get modalFormLoginEmail() {
    return this.validatingForm.get('modalFormLoginEmail');
  }

  get modalFormLoginPassword() {
    return this.validatingForm.get('modalFormLoginPassword');
  }
}

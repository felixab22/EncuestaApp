/* cSpell:disable */
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from './pages/pages.component';
import { BienvenidosComponent } from './pages/bienvenidos/bienvenidos.component';

const routes: Routes = [
  {
    path: '',
    component: BienvenidosComponent
  },
  {
    path: 'Posgrado',
    component: PagesComponent,
    children:
      [
        {
          path: 'Inicio',
          loadChildren: './pages/inicio/inicio.module#InicioModule'
        }
      ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
